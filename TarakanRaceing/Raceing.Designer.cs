﻿namespace TarakanRaceing
{
    partial class Raceing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBoxFinish = new System.Windows.Forms.PictureBox();
            this.labelFinishLine = new System.Windows.Forms.Label();
            this.buttonFirst = new System.Windows.Forms.Button();
            this.buttonSecond = new System.Windows.Forms.Button();
            this.buttonThird = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFinish)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxFinish
            // 
            this.pictureBoxFinish.BackColor = System.Drawing.Color.Crimson;
            this.pictureBoxFinish.Location = new System.Drawing.Point(744, 12);
            this.pictureBoxFinish.Name = "pictureBoxFinish";
            this.pictureBoxFinish.Size = new System.Drawing.Size(3, 426);
            this.pictureBoxFinish.TabIndex = 0;
            this.pictureBoxFinish.TabStop = false;
            // 
            // labelFinishLine
            // 
            this.labelFinishLine.AutoSize = true;
            this.labelFinishLine.Location = new System.Drawing.Point(684, 12);
            this.labelFinishLine.Name = "labelFinishLine";
            this.labelFinishLine.Size = new System.Drawing.Size(54, 13);
            this.labelFinishLine.TabIndex = 1;
            this.labelFinishLine.Text = "FinishLine";
            // 
            // buttonFirst
            // 
            this.buttonFirst.Location = new System.Drawing.Point(36, 49);
            this.buttonFirst.Name = "buttonFirst";
            this.buttonFirst.Size = new System.Drawing.Size(48, 23);
            this.buttonFirst.TabIndex = 2;
            this.buttonFirst.Text = "First";
            this.buttonFirst.UseVisualStyleBackColor = true;
            // 
            // buttonSecond
            // 
            this.buttonSecond.Location = new System.Drawing.Point(36, 202);
            this.buttonSecond.Name = "buttonSecond";
            this.buttonSecond.Size = new System.Drawing.Size(48, 23);
            this.buttonSecond.TabIndex = 3;
            this.buttonSecond.Text = "Second";
            this.buttonSecond.UseVisualStyleBackColor = true;
            // 
            // buttonThird
            // 
            this.buttonThird.Location = new System.Drawing.Point(36, 339);
            this.buttonThird.Name = "buttonThird";
            this.buttonThird.Size = new System.Drawing.Size(48, 23);
            this.buttonThird.TabIndex = 4;
            this.buttonThird.Text = "Third";
            this.buttonThird.UseVisualStyleBackColor = true;
            // 
            // buttonStart
            // 
            this.buttonStart.BackColor = System.Drawing.Color.DarkGreen;
            this.buttonStart.Location = new System.Drawing.Point(650, 406);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(88, 32);
            this.buttonStart.TabIndex = 5;
            this.buttonStart.Text = "START";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // Timer
            // 
            this.Timer.Interval = 5;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // Raceing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.YellowGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonThird);
            this.Controls.Add(this.buttonSecond);
            this.Controls.Add(this.buttonFirst);
            this.Controls.Add(this.labelFinishLine);
            this.Controls.Add(this.pictureBoxFinish);
            this.Name = "Raceing";
            this.Text = "Raceing";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFinish)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxFinish;
        private System.Windows.Forms.Label labelFinishLine;
        private System.Windows.Forms.Button buttonFirst;
        private System.Windows.Forms.Button buttonSecond;
        private System.Windows.Forms.Button buttonThird;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Timer Timer;
    }
}

