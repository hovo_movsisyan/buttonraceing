﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TarakanRaceing
{
    public partial class Raceing : Form
    {

        public Raceing()
        {
            InitializeComponent();
        }

        public static List<int> NumberGenerator()
        {
            Random rnd = new Random();
            List<int> numbers = new List<int>();
            while (numbers.Count < 3)
            {
                int numrand = rnd.Next(1, 4);
                if (!numbers.Contains(numrand))
                    numbers.Add(numrand);
            }
            return numbers;
        }
        readonly List<int> newlist = NumberGenerator();

        private void Timer_Tick(object sender, EventArgs e)
        {
            ButtonStart_Click(sender, e);
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            Timer.Start();
            if (buttonFirst.Location.X > 700)
            {
                buttonFirst.Visible = false;
                Timer.Stop();
                MessageBox.Show("First WIN!!");
            }
            else
            {
                buttonFirst.Location = new Point(buttonFirst.Location.X + newlist[0],
                buttonFirst.Location.Y);
            }
            if (buttonSecond.Location.X > 700)
            {
                buttonSecond.Visible = false;
                Timer.Stop();
                MessageBox.Show("Second WIN!!");
            }
            else
            {
                buttonSecond.Location = new Point(buttonSecond.Location.X + newlist[1],
                buttonSecond.Location.Y);
            }
            if (buttonThird.Location.X > 700)
            {
                buttonThird.Visible = false;
                Timer.Stop();
                MessageBox.Show("Third WIN!!");
            }
            else
            {
                buttonThird.Location = new Point(buttonThird.Location.X + newlist[2],
                buttonThird.Location.Y);
            }
        }

    }
}
